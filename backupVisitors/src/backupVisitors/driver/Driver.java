/**
 * 
 */
package backupVisitors.driver;

import backupVisitors.myTree.Node;
import backupVisitors.util.FileDisplayInterface;
import backupVisitors.util.FileProcessor;
import backupVisitors.util.Results;
import backupVisitors.util.TreeBuilder;
import backupVisitors.visitor.FullTimeStatusVisitorImpl;
import backupVisitors.visitor.IdenticalRecordsVisitorImpl;
import backupVisitors.visitor.SortedRecordsVisitorImpl;
import backupVisitors.visitor.StatsVisitorImpl;

/**
 * @author prathamesh
 *
 */
public class Driver {
	public static void main(String[] args) {
		String[] separate = null;
		String inputFile = null;
		String statsFile = null;
		String sorted = null;
		String deleteFile = null;
		String identicalFile = null;
		if (args.length != 5) {
			System.err.println("Expected five arguments");
			System.exit(1);
		} else {
			inputFile = args[0];
			deleteFile = args[1];
			statsFile = args[2];
			sorted = args[3];
			identicalFile = args[4];

		}

		FileProcessor fp = new FileProcessor(inputFile);
		String line = null;
		FileDisplayInterface res_orig = new Results();
		FileDisplayInterface res_backup_1 = new Results();
		FileDisplayInterface res_backup_2 = new Results();
		TreeBuilder bst_orig = new TreeBuilder();
		TreeBuilder bst_backup_1 = new TreeBuilder();
		TreeBuilder bst_backup_2 = new TreeBuilder();
		FullTimeStatusVisitorImpl fulltime = new FullTimeStatusVisitorImpl();
		SortedRecordsVisitorImpl sortedRecord = new SortedRecordsVisitorImpl();
		StatsVisitorImpl stats = new StatsVisitorImpl();
		IdenticalRecordsVisitorImpl identical = new IdenticalRecordsVisitorImpl();
		while ((line = fp.readLine()) != null) {
			// println(line + "\n");
			line.split(":");
			separate = line.split(":");
			String course = separate[1];
			String number = separate[0];
			if ((course.length() == 1) && course.matches("[A-K?]")) {
				int bNumber = Integer.parseInt(number);
				Node check = bst_orig.find(bNumber);
				Node orig_node = null;
				Node backup_1 = null;
				Node backup_2 = null;
				if (check == null) {
					orig_node = new Node(bNumber, course);
					bst_orig.insert(orig_node);
					if (orig_node instanceof Node) {
						backup_1 = (Node) orig_node.Clone();
						bst_backup_1.insert(backup_1);
						backup_2 = (Node) orig_node.Clone();
						bst_backup_2.insert(backup_2);
						orig_node.registerObserver(backup_1);
						orig_node.registerObserver(backup_2);
					}
				} else if (!(check.courses.contains(course))) {
					check.courses.add(course);
					check.notifyObserver(check.getObservers(), "insert", course);
				}
			}
		}

		FileProcessor fp1 = new FileProcessor(deleteFile);
		String line1 = null;
		String[] seperate1 = null;
		while ((line1 = fp1.readLine()) != null) {
			seperate1 = line1.split(":");
			String deleteCourse = seperate1[1];
			if ((deleteCourse.length() == 1) && deleteCourse.matches("[A-K?]")) {
				String deleteNumber = seperate1[0];
				int deleteBNumber = Integer.parseInt(deleteNumber);
				Node check1 = bst_orig.find(deleteBNumber);
				if (check1 != null) {
					check1.courses.remove(deleteCourse);
					check1.notifyObserver(check1.getObservers(), "delete", deleteCourse);
				}
			}
		}
		bst_orig.accept(fulltime);
		bst_backup_1.accept(stats, res_backup_1);
		bst_backup_2.accept(sortedRecord, res_backup_2);
		bst_orig.accept(identical, res_orig);
		res_orig.writeToFile(identicalFile);
		res_backup_1.writeToFile(statsFile);
		res_backup_2.writeToFile(sorted);
	}
}