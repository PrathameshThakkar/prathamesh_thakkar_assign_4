/**
 * 
 */
package backupVisitors.visitor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import backupVisitors.myTree.Node;
import backupVisitors.util.FileDisplayInterface;
import backupVisitors.util.Results;
import backupVisitors.util.TreeBuilder;

/**
 * @author prathamesh
 *
 */
/**
 * @author prathamesh
 *
 */
public class SortedRecordsVisitorImpl implements TreeVisitorI {
	private Node sortedTraversal;
	private ArrayList<Node> nm;
	private FileDisplayInterface results;

	public SortedRecordsVisitorImpl() {
		// TODO Auto-generated constructor stub
		nm = new ArrayList<>();
	}

	/**
	 * @param tree,tree to be visited
	 * @param resultsIn, result object to be written to
	 */

	@Override
	public void visit(TreeBuilder tree, FileDisplayInterface resultsIn) {
		// TODO Auto-generated method stub
		results = resultsIn;

		sortedTraversal = tree.root;
		sortedDisplay(sortedTraversal);
		Collections.sort(nm, NodeComparator);

		String number = "";
		for (Node n : nm) {
			boolean flag = false;
			String temporary = "";
			String buf = "";
			number = "" + n.bNumber + ": ";
			for (String str : n.courses) {
				if (!flag) {
					temporary = str;
					flag = true;
				} else {
					buf = ", " + str;
					temporary += buf;
				}
			}
			((Results) results).storeNewResult(number + temporary);
		}
	}

	/**
	 * @param temp,
	 *            root node of the tree to be traversed
	 */
	public void sortedDisplay(Node temp) {
		// boolean flag = false;
		if (temp != null) {
			sortedDisplay(temp.left);
			nm.add(temp);
			sortedDisplay(temp.right);
		}
	}

	public static Comparator<Node> NodeComparator = new Comparator<Node>() {
		public int compare(Node n1, Node n2) {
			int bNumber1 = n1.bNumber;
			int bNumber2 = n2.bNumber;

			return bNumber2 - bNumber1;
		}
	};
}
