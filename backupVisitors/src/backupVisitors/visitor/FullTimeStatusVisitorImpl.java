/**
 * 
 */
package backupVisitors.visitor;

import backupVisitors.myTree.Node;
import backupVisitors.util.FileDisplayInterface;
import backupVisitors.util.TreeBuilder;

/**
 * @author prathamesh
 *
 */
public class FullTimeStatusVisitorImpl implements TreeVisitorI {
	private Node traversal = null;

	@Override
	public void visit(TreeBuilder tree, FileDisplayInterface resultsIn) {
		traversal = tree.root;
		traversalDisplay(traversal);
	}

	public void traversalDisplay(Node temp) {
		boolean flag = false;
		if (temp != null) {
			traversalDisplay(temp.left);
			if (temp.courses.size() < 3) {
				temp.courses.add("S");
				temp.notifyObserver(temp.getObservers(), "insert", "S");
			}
			for (String s : temp.courses) {
				if (!flag) {
					flag = true;
				} else {
				}
			}
			traversalDisplay(temp.right);
		}
	}

}
