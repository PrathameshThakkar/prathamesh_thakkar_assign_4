/**
 * 
 */
package backupVisitors.visitor;

import java.util.ArrayList;
import java.util.Collections;

import backupVisitors.myTree.Node;
import backupVisitors.util.FileDisplayInterface;
import backupVisitors.util.Results;
import backupVisitors.util.TreeBuilder;

/**
 * @author prathamesh
 *
 */
public class StatsVisitorImpl implements TreeVisitorI {
	private Node statsTraversal = null;
	private ArrayList<Integer> stats;
	private FileDisplayInterface results;

	public StatsVisitorImpl() {
		stats = new ArrayList<>();
	}

	/**
	 * @param tree,
	 *            tree to be visited
	 * @param resultsIn,
	 *            result object to be written to
	 */

	@Override
	public void visit(TreeBuilder tree, FileDisplayInterface resultsIn) {
		// TODO Auto-generated method stub
		results = resultsIn;
		statsTraversal = tree.root;
		statsDisplay(statsTraversal);

		Collections.sort(stats);
		int sum = 0;
		for (Integer val : stats) {
			sum = sum + val;
		}
		float Average = (float) sum / stats.size();
		((Results) results).storeNewResult("Average is : " + Average);

		float median;
		int total;
		if (stats.size() % 2 == 0) {
			total = stats.get((stats.size() / 2) - 1) + stats.get((stats.size() / 2));
			median = (float) total / 2;
			((Results) results).storeNewResult("Median is: " + median);

		} else {
			median = stats.get(stats.size() / 2);
			((Results) results).storeNewResult("Median is: " + median);
		}
	}

	/**
	 * @param temp,
	 *            root node of the tree to be traversed
	 */
	public void statsDisplay(Node temp) {
		String s = "S";
		if (temp == null)
			return;

		if (temp.courses.contains(s)) {
			stats.add(temp.courses.size() - 1);
		} else {
			stats.add(temp.courses.size());
		}
		statsDisplay(temp.left);
		statsDisplay(temp.right);
	}
}
