/**
 * 
 */
package backupVisitors.visitor;

import backupVisitors.util.FileDisplayInterface;
import backupVisitors.util.TreeBuilder;

/**
 * @author prathamesh
 *
 */
public interface TreeVisitorI {

	public void visit(TreeBuilder tree, FileDisplayInterface resultsIn);

}
