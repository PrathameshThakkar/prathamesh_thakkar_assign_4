/**
 * 
 */
package backupVisitors.visitor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import backupVisitors.myTree.Node;
import backupVisitors.util.FileDisplayInterface;
import backupVisitors.util.Results;
import backupVisitors.util.TreeBuilder;

/**
 * @author prathamesh
 *
 */
public class IdenticalRecordsVisitorImpl implements TreeVisitorI {
	private ArrayList<Node> record;
	private Node visitor;
	private FileDisplayInterface results;

	public IdenticalRecordsVisitorImpl() {
		// TODO Auto-generated constructor stub
		record = new ArrayList<Node>();
	}

	/**
	 * @param tree, tree to be visited
	 * @param resultsIn, result object to be written to
	 */
	@Override
	public void visit(TreeBuilder tree, FileDisplayInterface resultsIn) {
		results = resultsIn;
		visitor = tree.root;
		Map<String, ArrayList<Integer>> map = new HashMap<String, ArrayList<Integer>>();

		identicalRecordsVisitor(visitor);
		for (Node n : record) {
			String strTemp = "";

			Collections.sort(n.courses);
			for (int i = 0; i < n.courses.size(); i++) {
				strTemp += n.courses.get(i);
			}
			n.courses.add(strTemp);
			String buf = n.courses.get((n.courses.size()) - 1);

			if (map.containsKey(buf)) {
				map.get(buf).add(n.bNumber);
			} else {
				ArrayList<Integer> arr = new ArrayList<>();
				arr.add(n.bNumber);
				map.put(buf, arr);
			}
		}
		int group = 1;
		String elements = "Contains B-Number- ";
		ArrayList<Integer> str = new ArrayList<>();
		for (Map.Entry<String, ArrayList<Integer>> entry : map.entrySet()) {
			boolean flag = false;
			str = entry.getValue();
			String temporary = "";
			String print = "";
			((Results) results).storeNewResult("Group number " + group + ":");

			for (Integer i : str) {
				if (!flag) {
					temporary = "" + i;
					flag = true;
				} else {
					print = ", " + i + "";
					temporary += print;
				}

			}
			((Results) results).storeNewResult(elements + temporary);
			group++;
		}
	}

	/**
	 * @param temp,
	 *            root node of the tree to be traversed
	 */
	public void identicalRecordsVisitor(Node temp) {
		if (temp != null) {
			identicalRecordsVisitor(temp.left);
			record.add(temp);
			identicalRecordsVisitor(temp.right);
		}
	}
}
