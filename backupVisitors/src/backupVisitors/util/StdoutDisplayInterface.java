/**
 * 
 */
package backupVisitors.util;

/**
 * @author prathamesh
 *
 */
public interface StdoutDisplayInterface {
	void writeToStdout();
}
