/**
 * 
 */
package backupVisitors.myTree;

import java.util.ArrayList;

import backupVisitors.myTree.Node;
import backupVisitors.myTree.ObserverI;
import backupVisitors.myTree.SubjectI;

/**
 * @author prathamesh
 *
 */
public class Node implements SubjectI, ObserverI, Cloneable {
	public int bNumber;
	private String courseWork;
	public ArrayList<String> courses = null;
	public Node left;
	public Node right;
	private ArrayList<Node> observers;

	/**
	 * @return observers of the original node
	 */
	public ArrayList<Node> getObservers() {
		return observers;
	}

	/**
	 * @return String, coursework
	 */
	public String getCourseWork() {
		return courseWork;
	}

	/**
	 * @param courseWork,
	 *            sets the coursework of the current object
	 */
	public void setCourseWork(String courseWork) {
		this.courseWork = courseWork;
	}

	/**
	 * @param bNumberIn,
	 *            B-number of the node
	 * @param coursesIn,
	 *            Course of the node
	 */
	public Node(int bNumberIn, String coursesIn) {
		bNumber = bNumberIn;
		courses = new ArrayList<String>();
		courseWork = coursesIn;
		courses.add(courseWork);
		left = null;
		right = null;
		observers = new ArrayList<Node>();
	}

	/**
	 * @return Node, copy of the node
	 */
	public Object Clone() {
		Node copy = new Node(bNumber, courseWork);
		return copy;
	}

	/**
	 * @return size of the ArrayList of observers
	 */
	public int sizeofReference() {
		return observers.size();
	}

	/**
	 * @param o,
	 *            Node to be updated
	 * @param type,
	 *            Type of operation to be performed on that node @value, value
	 *            to be inserted or removed from the node
	 */
	@Override
	public void update(Node o, String type, String value) {
		// TODO Auto-generated method stub

		if (type.equals("insert")) {
			o.courses.add(value);
		} else if (type.equals("delete")) {
			o.courses.remove(value);
		}
	}

	/**
	 * @param o,
	 *            register observer with the original node(Subject)
	 */
	public void registerObserver(ObserverI o) {
		observers.add((Node) o);
		for (Node obj : observers) {
			// System.out.println(o);
		}
		// System.out.println("Size of observers: " +observers.size());
	}

	/**
	 * @param o,
	 *            unregister a particular observer from Subject
	 */
	@Override
	public void removeObserver(ObserverI o) {
		// TODO Auto-generated method stub
		observers.remove(o);

	}

	/**
	 * @param arr,
	 *            to notify all the observers in the ArrayList
	 * @param type,
	 *            type of operation to be performed on observers
	 * @param value,
	 *            to be inserted or removed
	 */
	@Override
	public void notifyObserver(ArrayList<Node> arr, String type, String value) {
		// TODO Auto-generated method stub
		for (Node node : arr) {
			update(node, type, value);

		}
	}

}
