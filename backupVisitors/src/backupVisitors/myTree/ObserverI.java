/**
 * 
 */
package backupVisitors.myTree;

import backupVisitors.myTree.Node;

/**
 * @author prathamesh
 *
 */
public interface ObserverI {
	void update(Node o, String type, String value);
}
